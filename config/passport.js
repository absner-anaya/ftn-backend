
const   passport = require('passport'),
        JwtStrategy = require('passport-jwt').Strategy,
        LocalStrategy = require('passport-local').Strategy;

module.exports = {
    
    http: {
        customMiddleware: function(app) {
            console.log('Express middleware for passport');
            app.use( passport.initialize() );
            app.use( passport.session() );
        }
    }
};