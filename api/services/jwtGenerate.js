const jwt = require('jsonwebtoken');

const EXPIRES_IN_MINUTES = 60 * 24;
const SECRET = process.env.tokenSecret || "4ukI0uIVnB3iI1yxj646fVXSE3ZVk4doZgz6fTbNg7jO41EAtl20J5F7Trtwe7OM";
const ALGORITHM = "HS256";
const ISSUER = "nozus.com";
const AUDIENCE = "nozus.com";


module.exports = {
    createToken: function(user) {
        return jwt.sign(
            //user: user.toJSON()
            user
        ,
            SECRET,
        {
            algorithm: ALGORITHM,
            issuer: ISSUER,
            expiresIn: 60 * 60 * 24,
            audience: AUDIENCE
        }
        )
    },

    verify: function(token, cb) {
        return jwt.verify(
            token,
            SECRET,
            {},
            cb
        );
    },
    
}