/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    find: function (req, res) {
        
        User.find().exec(function (err, users) {
            if(err) return res.json({ err: err }, 500);
            else res.json(users);
        });
    },

    findOne: function (req, res) {
      User.findOne({ id: req.params.id }).exec(function (err, user) {
        if(err) return res.json({ err: err }, 500);
        else res.json(user);
      });
    },

    create: function (req, res){
        User.create(req.body
        ).exec(function (err, data) {
            if(err) return res.json({ err: err }, 500);
            else res.json({
                code: 200,
                message: 'success',
                content: data
            });
        })
    },

    update: function (req, res) {
        User.update(
            {id: req.params.id},
            req.body
        ).exec(function (err, data) {
            if(err){ 
                return res.json({ err: err }, 500);
            }else { 
                
                return res.json({
                    code: 200,
                    message: 'Update data',
                    content: data[0]
                });
            }
        });
    },

    destroy: function (req, res) {
        User.destroy({id: req.params.id}).exec(function (err, user) {
            if(err) return res.json({ err: err }, 500);
            else res.json({
                code: 200,
                message: 'user delete'
            });
        });
    },

    
};

module.exports.blueprints = {
    actions: true,
    rest: true,
    shortcuts: true
};

