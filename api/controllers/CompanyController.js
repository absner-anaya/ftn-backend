/**
 * CompanyController
 *
 * @description :: Server-side logic for managing companies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    find: function (req, res) {
        Company.find().exec(function (err, users) {
            if(err) return res.json({ err: err }, 500);
            else res.json(users);
        });
    },

    findOne: function (req, res) {
      Company.findOne({ id: req.params.id }).exec(function (err, data) {
        if(err) return res.json({ err: err }, 500);
        else res.json(data);
      });
    },

    create: function(req, res) {
        Company.create(req.body).exec(function(err, data) {
            if(err) return res.json({ err: err }, 500);
            else res.json({
                code: 200,
                message: 'success',
                content: data
            });
        })
    },

    update: function(req, res) {
        Company.update(
            {id: req.params.id},
            req.body
        ).exec(function (err, data) {
          if(err) return res.json({ err: err }, 500);
          else res.json(data);
        });
    },

    destroy: function(req, res) {
        Company.destroy({id: req.params.id}).exec(function (err, data) {
            if(err) return res.json({ err: err }, 500);
            else res.json({
                code: 200,
                message: 'user delete',
                content: data
            });
        });
    }
	
};
module.exports.blueprints = {
    actions: true,
    rest: true,
    shortcuts: true
};

