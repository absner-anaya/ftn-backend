/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport'),
      jwt = require('jsonwebtoken');

module.exports = {


    
    login: function(req, res) {
        
        passport.authenticate('local', function(err, user, info) {
            if ( (err) || (!user) ) {
                return res.status(200).send({
                    code: 400,
                    message: 'User failed'
                });
                res.send(err);
            }
            req.logIn(user, function(err) {
                if(err) res.send(err);

                return res.status(200).send({
                    code: 200,
                    message: 'Login successFull ',
                    data: {
                        user,
                        token: jwtGenerate.createToken({id: user.id})
                    }

                })

            });
        })(req, res);
    },
    logout: function(req, res) {
        req.logout();
        req.session.destroy(function (err) {
            if (err) {
                return res.status(200).send({
                    code: 400,
                    message: 'Logout failed'
                });
            }
            req.session = null;
            return res.status(200).send({
                    code: 200,
                    message: 'Logout successFull ',
                })
        });
    },
    send: function(req, res) {

        sails.hooks.email.send(
            "testEmail", {
                recipientName: "Joe",
                senderName: "Sue",

            }, {
                to: "lissethramirez876@gmail.com",
                subject: "Hi there"
            },
            function(err) {
                if (err) {return res.serverError(err);}
                return res.json({
                    code: 200,
                    message: 'Correo enviado'
                });
            }
        );
    },
    resetPassword: function (req, res) {
        var params = req.params.all(),
            email= params.email,
            url = sails.getBaseurl();
        User.findOne({ email:email }).exec(function (err, em){
            if(err) return res.json({ err: err }, 500);
            if (!em) {
                return res.json({
                    code: 404,
                    message:'Email no encontrado.'
                });
            }
            var token = jwtGenerate.createToken({id: em.id});
            //var token = jwtGenerate.verify({id: em.id});
            
            var r = url + '/cPassword?email=' + em.email + '&token=' + token;

            sails.hooks.email.send(
                "testEmail", {
                    recipientName: em.first_name + ' ' + em.last_name,
                    url: r
                }, {
                    to: em.email,
                    subject: "Olvidaste tu contraseña"
                },
                function(err) {
                    if (err) {return res.serverError(err);}
                    return res.json({
                        code: 200,
                        message: 'Correo enviado',
                        url: r
                    });
                }
            );
            /*return res.json({
                    code: 200,
                    message: 'Url create',
                    content: r
                });*/
        });
    },
    cPassword  : function (req, res){
        var params = req.params.all(),
            email= params.email,
            token = params.token;

        jwtGenerate.verify(token, function (err, token) {
            if (err) return res.json(401, {err: 'Invalid Token!'});
            req.token = token; // This is the decrypted token or the payload you provided
            User.findOne({ id:token.id }).exec(function (err, user){
                if(err) return res.json({ err: err }, 500);
                if (!user) {
                    return res.json({
                        code: 404,
                        message:'Email no encontrado.'
                    });
                }
                if (email == user.email){
                    return res.json({
                            code: 200,
                            message: 'Mostar form para cambiar password',
                            token: token,
                            user:user
                    });
                }else{
                    return res.json('Token no coincide.');
                }
            });
        });
    },
    changePassword : function (req, res){
        var params = req.params.all(),
            email_= params.email,
            token = params.token,
            password = params.password;
        
        if (token){
            jwtGenerate.verify(token, function (err, token) {
                    if (err) return res.json(401, {err: 'Invalid Token!'});
                    req.token = token; // This is the decrypted token or the payload you provided
                    User.findOne({ email: email_ }).exec(function (err, user){
                        if(err) return res.json({ err: err }, 500);
                        if (!user) {
                            return res.json('Email no encontrado.');
                        }
                        if (email_ == user.email){
                            /*var date  = user.mdpTokenTimestamp.getTime();
                            var now   = new Date().getTime();
                            var limit = req._sails.config.passport.passwordResetTokenValidity || 86400000;//More than 24h token is invalid
                            */
                            /*if (now - date >= limit) {//More than 24h token is invalid
                                return res.json({ code: 403, message: 'token is invalid' });
                            }else{*/
                                User.update(user.id, {password : req.param("password")}).exec(function (err){
                                    if (err) return res.negotiate(err);
                                    return res.json({
                                        code: 200,
                                        message: 'password cambiada',
                                    });
                                });
                            //}
                        }
                    });
                });
        }
    }
};

module.exports.blueprints = {
    actions: true,
    rest: true,
    shortcuts: true
};
