/**
 * Pedidos.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	empresa: {
      type: 'string'
    },
    direccion: {
      type: 'string'
    },
    departamento_id: {
      model: 'departamentos',
      via: 'id',
    },
    municipio_id: {
      model: 'municipios',
      via: 'id',
    },
    nit: {
      type: 'string'
    },
    persona: {
      type: 'string'
    },
    cargo: {
      type: 'string'
    },
    telefono: {
      type: 'string'
    },
    celular: {
      type: 'string'
    },
    fax: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    vip_id: {    // 1 S, 2 N, 3 Estructuras, 4 Exporadico, 5 Grandes clientes
      model: 'pedidos_vip',
      via: 'id',
    },
    plantilla_id: {   // 1 Exposistemas, 2 ventadeportapendones.com, 3 Distribuidores Estrada, 4 Rollup, 5 Marco
      model: 'pedidos_plantillas',
      via: 'id',
    },
    elaborado: {
      type: 'string'
    },
    pendiente: {
      type: 'boolean', // 0 no -- 1 si
      defaultsTo: false
    },
    estado_id: {  // 1 cotizacion, 2 cancelada/modificada, 3 pendiente, 4 cotización excenta, 5 factura emitida
      model: 'pedidos_estados',
      via: 'id',
      defaultsTo: '1'
    },
    pedidos_seguimientos: {
      collection: 'pedidos_seguimientos',
      via: 'owner'
    },
    pedidos_recordatorios: {
      collection: 'pedidos_recordatorios',
      via: 'owner'
    },
    pedidos_alertas: {
      collection: 'pedidos_alertas',
      via: 'owner'
    },
    opciones_factura_id: {  // 1 Original, 2 Pago el 50%, 3 Pago el 100%, 4 Generar factura, 5 Generar factura sin IVA
      model: 'opciones_factura',
      via: 'id',
      defaultsTo: '1'
    },
    puntos: {
    	type:'integer'
    },
    pmo: {
      type: 'boolean', // 0 no -- 1 si
      defaultsTo: false
    },
    tracking: {
      type: 'string'
    },
    aviso: {    // sin avisos por defecto
      type: 'string',
      enum: ['0','1','2'],
      defaultsTo: '0'
    },
    status: {
      type: 'boolean', // 0 inactivo -- 1 activo
      defaultsTo: true
    }
  }
};
