/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');

module.exports = {

  schema: true,
  attributes: {

    rut: {
      type: 'string',
      unique: true
    },
    company_id: {
      model: 'company',
      via: 'id',
      defaultsTo: null
    },
    first_name: {
      type: 'string'
    },
    last_name: {
      type: 'string'
    },
    username: {
      type: 'string',
      unique: true
    },
    phone: {
      type: 'string'
    },
    email: {
      type: 'string',
      unique: true
    },
    password: {
      type: 'string'
    },
    lang: {
      type: 'string',
      enum: ['en','es','pl','de','fr'],
      defaultsTo: 'es'
    },
    type_id: {
      model: 'usertype',
      via: 'id',
      defaultsTo: 1
    },
    status: {
      type: 'boolean', // 0 inactivo -- 1 activo
      defaultsTo: false
    },

    toJSON: function() {
      let obj = this.toObject();
      delete obj.password;
      return obj;
    }

  },

  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(user.password, salt, function (err, hash){
        if (err){
          console.log(err);
          cb(err);
        }else{
          user.password = hash;
          cb(null, user);
        }
      })
    })
  },
  beforeUpdate: function(user, cb) {
    if (user.password === undefined){
      cb(null, user);
    }else{
      bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(user.password, salt, function (err, hash){
          if (err){
            console.log(err);
            cb(err);
          }else{
            user.password = hash;
            cb(null, user);
          }
        })
      })
    }    
  }
};

//user/create?rut=rutliss&first_name=Lisseth&last_name=Ramirez&username=liss23&phone=04120851284&email=lisseth@gmail.com&password=123654789&lang=es
