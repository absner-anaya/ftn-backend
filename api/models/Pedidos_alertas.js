/**
 * Pedidos_alertas.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	titulo: {
      type: 'string'
    },
  	descripcion: {
      type: 'string'
    },
    estado: {
      type: 'boolean', // 0 inactivo -- 1 activo
      defaultsTo: false
    },
    destinatario: {
      type: 'string'
    },
    owner: {
      model: 'pedidos'
    }
  }
};

